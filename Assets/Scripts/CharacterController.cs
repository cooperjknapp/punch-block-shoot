﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour
{
    [SerializeField]
    private GameObject[] sfx; // oof boing pow
    [SerializeField]
    private int playerNumber;
    [SerializeField]
    private List<Sprite> sprites;
    [SerializeField]
    private string[] controls;  //FORMATTED ULDR PBS

    [SerializeField]
    private float maxSpeed;
    [SerializeField]
    private float acceleration;
    [SerializeField]
    private float deceleration;
    [SerializeField]
    private float jumpStrength;
    private float jumpPower;
    [SerializeField]
    private float jumpLoss;
    private bool jumping;
    [SerializeField]
    private float attackCooldown;

    [SerializeField]
    private List<GameObject> pbsPrefabs;

    [SerializeField]
    private GameObject healthBar;

    private float currentSpeedX;
    private int previousDirection = 1;
    private bool isOnGround;
    private int punchLevel;
    private int blockLevel;
    private int shootLevel;
    private float attackCooldownTimer;
    private int currentHealth;
    private float hitstunTimer;

    private float attackOffset = 0.8f;
    private Vector2 shootAngle = new Vector2(1, 0.15f).normalized;
    private float shootSpeed = 15;
    private int maxHealth = 10;
    private float maxHitstunDuration = 0.45f;
    private Vector2 knockbackAngle = new Vector2(1.5f, 2f).normalized;
    private Vector2 startingPos;
    bool firstFrame;
    bool upgraded;
    bool angry;

    private SpriteRenderer sr;
    private Rigidbody2D rb2d;
    private GameManager gm;

    public int[] Levels {
        get {
            int[] levels = new int[3];
            levels[0] = punchLevel;
            levels[1] = blockLevel;
            levels[2] = shootLevel;
            return levels;
        }
    }

    // Start is called before the first frame update
    void Start() {
        sr = gameObject.GetComponent<SpriteRenderer>();
        rb2d = gameObject.GetComponent<Rigidbody2D>();
        gm = GameObject.Find("Game Manager").GetComponent<GameManager>();

        punchLevel = 0;
        blockLevel = 0;
        shootLevel = 0;
        attackCooldownTimer = 0;
        currentHealth = maxHealth;
        firstFrame = true;
        angry = false;
        jumping = false;
    }

    // Update is called once per frame
    void Update() {
        FirstFrame();

        bool left = Input.GetKey(controls[1]);
        bool right = Input.GetKey(controls[3]);
        bool jump = Input.GetKeyDown(controls[0]);
        bool jumpHold = Input.GetKey(controls[0]);
        bool punch = Input.GetKeyDown(controls[4]);
        bool block = Input.GetKeyDown(controls[5]);
        bool shoot = Input.GetKeyDown(controls[6]);


        if(gm.GameState.Equals("UPGRADE")) {
            HandleUpgrades(punch, block, shoot);
        } else if(gm.GameState.Equals("PLAY")) {
            HandleHitstun();
            if (hitstunTimer <= 0) {
                PlayerMovement(left, right);
                PlayerJumpment(jump, jumpHold);
                PlayerAttack(punch, block, shoot);
            }
        } else if(gm.GameState.Equals("WAIT")) {

        }
    }

    private void HandleUpgrades(bool punch, bool block, bool shoot) {
        attackCooldownTimer -= Time.deltaTime;

        if (!upgraded && attackCooldownTimer < 0) {
            if (punch) {
                if (punchLevel < 2) {
                    punchLevel++;
                    upgraded = true;
                    gm.PlayerReadyUp(gameObject.name, playerNumber);
                }
            } else if (block) {
                if (blockLevel < 2) {
                    blockLevel++;
                    upgraded = true;
                    gm.PlayerReadyUp(gameObject.name, playerNumber);
                }
            } else if (shoot) {
                if (shootLevel < 2) {
                    shootLevel++;
                    upgraded = true;
                    gm.PlayerReadyUp(gameObject.name, playerNumber);
                }
            }
        }
    }

    private void FirstFrame() {
        if(firstFrame) {
            startingPos = transform.position;
            firstFrame = false;
        }
    }

    private void HandleHitstun() {
        hitstunTimer -= Time.deltaTime;
    }

    private void PlayerAttack(bool punch, bool block, bool shoot) {
        attackCooldownTimer -= Time.deltaTime;

        if (attackCooldownTimer < 0) {
            if(punch) {
                GameObject punchObject = Instantiate(pbsPrefabs[0], new Vector2(transform.position.x + (attackOffset * previousDirection),
                                                                                transform.position.y), Quaternion.identity);
                punchObject.transform.parent = gameObject.transform;
                punchObject.GetComponent<PunchController>().Setup(punchLevel, previousDirection);
                attackCooldownTimer = attackCooldown;

                // ANIM STUFF
                angry = true;
                sr.sprite = sprites[2];
                if(previousDirection < 0) {
                    sr.flipX = true;
                } else {
                    sr.flipX = false;
                }
            } else if(block) {
                GameObject blockObject = Instantiate(pbsPrefabs[1], new Vector2(transform.position.x + (attackOffset * previousDirection),
                                                                                transform.position.y), Quaternion.identity);
                blockObject.transform.parent = gameObject.transform;
                BlockController bc = blockObject.GetComponent<BlockController>();
                bc.Setup(blockLevel, previousDirection);
                attackCooldownTimer = bc.Cooldown;

                // ANIM STUFF
                angry = true;
                sr.sprite = sprites[2];
                if (previousDirection < 0) {
                    sr.flipX = true;
                } else {
                    sr.flipX = false;
                }
            } else if(shoot) {
                Vector2 shootVelocity = shootAngle * shootSpeed;
                shootVelocity = new Vector2(shootVelocity.x * previousDirection, shootVelocity.y);
                GameObject shootObject = Instantiate(pbsPrefabs[2], new Vector2(transform.position.x + (attackOffset * previousDirection),
                                                                                transform.position.y), Quaternion.identity);
                shootObject.transform.parent = gameObject.transform;
                shootObject.GetComponent<ShootController>().Setup(shootLevel, shootVelocity);
                attackCooldownTimer = attackCooldown;

                // ANIM STUFF
                angry = true;
                sr.sprite = sprites[2];
                if (previousDirection < 0) {
                    sr.flipX = true;
                } else {
                    sr.flipX = false;
                }

                Instantiate(sfx[2], transform.position, Quaternion.identity);
            } else {
                angry = false;
            }
        }
    }

    private void PlayerMovement(bool left, bool right) {
        if (left && !right) { // WHILE HOLDING LEFT
            if (currentSpeedX > 0) {
                currentSpeedX = 0;
            }

            currentSpeedX -= acceleration * Time.deltaTime;
            if(!angry) sr.flipX = true;
            if (!angry) sr.sprite = sprites[1];

            previousDirection = -1;
        } else if (right && !left) { // WHILE HOLDING RIGHT
            if (currentSpeedX < 0) {
                currentSpeedX = 0;
            }

            currentSpeedX += acceleration * Time.deltaTime;
            if (!angry) sr.flipX = false;
            if (!angry) sr.sprite = sprites[1];

            previousDirection = 1;
        } else { // WHILE HOLDING BOTH OR NEITHER
            if (!angry) sr.flipX = false;
            if (!angry) sr.sprite = sprites[0];

            // Slow down
            float decel = deceleration * Time.deltaTime;
            if (currentSpeedX >= decel) {
                currentSpeedX -= decel;
            } else if (currentSpeedX <= -decel) {
                currentSpeedX += decel;
            } else {
                currentSpeedX = 0;
            }
        }
        // Clamp speed
        currentSpeedX = Mathf.Clamp(currentSpeedX, -maxSpeed, maxSpeed);

        rb2d.velocity = new Vector2(currentSpeedX, rb2d.velocity.y);
    }

    private void PlayerJumpment(bool jump, bool jumpHeld) {
        if (isOnGround && jump) {
            jumping = true;
            jumpPower = jumpStrength;
            rb2d.velocity = new Vector2(rb2d.velocity.x, jumpStrength);
            Instantiate(sfx[1], transform.position, Quaternion.identity);
        } else if (!jumpHeld) {
            jumping = false;
        }

        if (jumping && jumpHeld && jumpPower > 0) {
            rb2d.velocity = new Vector2(rb2d.velocity.x, rb2d.velocity.y + 15 * jumpPower * Time.deltaTime);
            jumpPower /= jumpLoss * Time.deltaTime + 1;
        } else if(jumpPower <= 0) {
            jumping = false;
        }
    }

    public void SetOnGround(bool isOnGround) {
        this.isOnGround = isOnGround;
    }
    
    public void GetHitBy(int damage, Vector2 other) {
        if(hitstunTimer >= -0.1f) {
            return;
        }

        currentHealth -= damage;
        if(currentHealth <= 0) {
            currentHealth = 0;
            HandleDeath();
        } else {
            Instantiate(sfx[0], transform.position, Quaternion.identity);
        }
        hitstunTimer = maxHitstunDuration;

        Vector2 knockbackVelocity = knockbackAngle * damage * 5;
        if (other.x > transform.position.x) {
            knockbackVelocity = new Vector2(-knockbackVelocity.x, knockbackVelocity.y);
        }
        rb2d.velocity = knockbackVelocity;

        healthBar.transform.localScale = new Vector2((float)currentHealth / (float)maxHealth, 1);
    }

    private void HandleDeath() {
        gm.PlayerDeath(gameObject.name, playerNumber);
    }

    public void ResetRound() {
        currentHealth = maxHealth;
        currentSpeedX = 0;
        transform.position = startingPos;
        rb2d.velocity = Vector2.zero;
        healthBar.transform.localScale = new Vector2(1, 1);
        angry = false;
        sr.sprite = sprites[0];
    }

    public void StartUpgrades() {
        currentSpeedX = 0;
        rb2d.velocity = Vector2.zero;
        upgraded = false;
        attackCooldownTimer = attackCooldown * 2;
    }
}
