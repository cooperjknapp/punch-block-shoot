﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionController : MonoBehaviour
{
    [SerializeField]
    protected List<Sprite> sprites;
    protected int level;
    public int Level {
        get {
            return level;
        }
    }
    protected SpriteRenderer sr;
}
