﻿using UnityEngine;

public class PunchController : ActionController
{
    [SerializeField]
    private float startingSpeed;
    [SerializeField]
    private float deceleration;
    private float speed;

    public void Setup(int level, int speedModifier) {
        sr = gameObject.GetComponent<SpriteRenderer>();
        this.level = level;
        sr.sprite = sprites[level];
        startingSpeed *= speedModifier;
        speed = startingSpeed;

        if(speedModifier < 0) {
            sr.flipX = true;
        }
    }

    void Update()
    {
        if(startingSpeed > 0) {
            transform.position = new Vector2(transform.position.x + (speed * Time.deltaTime), transform.position.y);
            speed -= deceleration * Time.deltaTime;

            if (speed <= -startingSpeed) {
                Destroy(gameObject);
            }
        } else {
            transform.position = new Vector2(transform.position.x + (speed * Time.deltaTime), transform.position.y);
            speed += deceleration * Time.deltaTime;

            if (speed >= -startingSpeed) {
                Destroy(gameObject);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if(collision.gameObject.tag.Equals("Player")) {
            collision.gameObject.GetComponent<CharacterController>().GetHitBy(level + 1, transform.position);
        }
    }
}
