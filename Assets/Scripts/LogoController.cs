﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogoController : MonoBehaviour
{
    private SpriteRenderer sr;
    [SerializeField]
    private List<Sprite> sprites;

    void Start() {
        sr = gameObject.GetComponent<SpriteRenderer>();
        sr.sprite = sprites[0];
    }

    public void UpdateSprite(int level) {
        if(level >= 3) {
            sr.enabled = false;
            return;
        }
        sr.sprite = sprites[level];
    }
}
