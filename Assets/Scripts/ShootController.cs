﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootController : ActionController
{
    [SerializeField]
    private GameObject explosionSfx;
    [SerializeField]
    private PhysicsMaterial2D bouncy;
    [SerializeField]
    GameObject explosionPrefab;
    private CircleCollider2D cc2d;
    private Rigidbody2D rb2d;
    private float maxFuseTimer = 2.5f;
    private float currentFuseTimer;

    public void Setup(int level, Vector2 startingVelocity) {
        sr = gameObject.GetComponent<SpriteRenderer>();
        cc2d = gameObject.GetComponent<CircleCollider2D>();
        rb2d = gameObject.GetComponent<Rigidbody2D>();
        this.level = level;
        sr.sprite = sprites[level];
        rb2d.velocity = startingVelocity;
        currentFuseTimer = maxFuseTimer;

        if(level > 0) {
            cc2d.sharedMaterial = bouncy;
        }
    }

    void Update()
    {
        currentFuseTimer -= Time.deltaTime;
        if(currentFuseTimer < 0) {
            Explode();
        }
    }

    private void Explode() {
        if (level == 2) {
            Instantiate(explosionPrefab, transform.position, Quaternion.identity);
            Instantiate(explosionSfx, transform.position, Quaternion.identity);
        }

        Destroy(gameObject);
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        if(collision.gameObject.tag.Equals("Ground")) {
            if(level == 0) {
                Destroy(gameObject);
            }
        } else if(collision.gameObject.tag.Equals("Player")) {
            if(level != 2) {
                collision.gameObject.GetComponent<CharacterController>().GetHitBy(level + 1, transform.position);
                Explode();
            }
        }
    }
}
