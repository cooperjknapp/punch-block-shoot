﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private GameObject[] sfx; // beep beep boom ui
    private Text countdownText;
    private List<CharacterController> players;
    private GameObject upgradeMenu;
    [SerializeField]
    private GameObject[] upgradeSubMenus;
    private LogoController[] logoControllers;
    private List<ObjectSlideIn> sliders;
    private ScoreboardController scoreCounter;
    private DontDestroyData dontDestroyData;

    private int roundNumber;
    private float roundBeginCountdown;
    private string gameState;
    private int lastNumber;

    private bool playerOneReady;
    private bool playerTwoReady;

    int[] scoreboard;

    public string GameState {
        get {
            return gameState;
        }
    }


    void Start() {
        players = new List<CharacterController>();
        countdownText = GameObject.Find("Timer Countdown").GetComponent<Text>();
        upgradeMenu = GameObject.Find("Upgrade Menu");
        scoreCounter = GameObject.Find("Score Counter").GetComponent<ScoreboardController>();
        logoControllers = new LogoController[6];
        AddLogoControllers();
        players.Add(GameObject.Find("Blue").GetComponent<CharacterController>());
        players.Add(GameObject.Find("Orange").GetComponent<CharacterController>());
        sliders = new List<ObjectSlideIn>();
        sliders.Add(GameObject.Find("Slider 1").GetComponent<ObjectSlideIn>());
        sliders.Add(GameObject.Find("Slider 2").GetComponent<ObjectSlideIn>());
        dontDestroyData = GameObject.Find("DontDestroyData").GetComponent<DontDestroyData>();

        scoreboard = new int[2];
        scoreboard[0] = 0;
        scoreboard[1] = 0;
        BeginRound(true);
    }

    void Update() {
        switch(gameState) {
            case "PLAY":
                break;

            case "WAIT":
                roundBeginCountdown -= Time.deltaTime;
                if (roundBeginCountdown <= 0) {
                    countdownText.gameObject.SetActive(false);
                    gameState = "PLAY";
                } else {
                    int countdownNumber = Mathf.RoundToInt(roundBeginCountdown);

                    if (lastNumber != countdownNumber) {
                        if(countdownNumber == 0) {
                            Instantiate(sfx[1], transform.position, Quaternion.identity);
                        } else {
                            Instantiate(sfx[0], transform.position, Quaternion.identity);
                        }
                    }

                    lastNumber = countdownNumber;

                    if(countdownNumber == 0) {
                        countdownText.text = "BEGIN";
                    } else {
                        countdownText.text = countdownNumber.ToString();
                    }
                }
                break;

            case "UPGRADE":
                if(playerOneReady && playerTwoReady) {
                    BeginRound(false);
                }
                break;
        }
    }

    private void BeginRound(bool firstRound) {
        roundBeginCountdown = 3.4f;
        gameState = "WAIT";

        if (!firstRound) {
            players[0].ResetRound();
            players[1].ResetRound();
        }
        countdownText.fontSize = 200;
        countdownText.gameObject.SetActive(true);
        upgradeMenu.SetActive(false);
    }

    private void EndRound() {
        playerOneReady = false;
        playerTwoReady = false;
        gameState = "UPGRADE";

        logoControllers[0].UpdateSprite(players[0].Levels[0] + 1);
        logoControllers[1].UpdateSprite(players[0].Levels[1] + 1);
        logoControllers[2].UpdateSprite(players[0].Levels[2] + 1);
        logoControllers[3].UpdateSprite(players[1].Levels[0] + 1);
        logoControllers[4].UpdateSprite(players[1].Levels[1] + 1);
        logoControllers[5].UpdateSprite(players[1].Levels[2] + 1);

        players[0].StartUpgrades();
        players[1].StartUpgrades();
        sliders[0].ReplayAnimation();
        sliders[1].ReplayAnimation();
        upgradeMenu.SetActive(true);
        upgradeSubMenus[0].SetActive(true);
        upgradeSubMenus[1].SetActive(true);
    }

    private void EndGame(int winner) {
        dontDestroyData.winner = winner;
        SceneManager.LoadScene(2);
    }

    public void PlayerDeath(string playerName, int playerCount) {
        scoreboard[(playerCount + 1) % 2]++;
        scoreCounter.ScorePlayer(playerCount % 2);

        if (scoreboard[0] >= 3) {
            EndGame(1);
            return;
        } else if (scoreboard[1] >= 3) {
            EndGame(0);
            return;
        }

        countdownText.fontSize = 100;
        countdownText.gameObject.SetActive(true);
        string totalText = playerName.ToUpper() + " DEFEAT CHOOSE UPGRADE";
        countdownText.text = totalText;
        Instantiate(sfx[2], transform.position, Quaternion.identity);

        EndRound();
    }

    public void PlayerReadyUp(string playerName, int playerCount) {
        upgradeSubMenus[playerCount - 1].SetActive(false);
        Instantiate(sfx[3], transform.position, Quaternion.identity);
        if(playerCount == 1) {
            playerOneReady = true;
        } else if(playerCount == 2) {
            playerTwoReady = true;
        }
    }

    private void AddLogoControllers() {
        logoControllers[0] = (GameObject.Find("Punch Logo 1").GetComponent<LogoController>());
        logoControllers[1] = (GameObject.Find("Block Logo 1").GetComponent<LogoController>());
        logoControllers[2] = (GameObject.Find("Shoot Logo 1").GetComponent<LogoController>());
        logoControllers[3] = (GameObject.Find("Punch Logo 2").GetComponent<LogoController>());
        logoControllers[4] = (GameObject.Find("Block Logo 2").GetComponent<LogoController>());
        logoControllers[5] = (GameObject.Find("Shoot Logo 2").GetComponent<LogoController>());
    }
}
