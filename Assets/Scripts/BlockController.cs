﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockController : ActionController
{
    [SerializeField]
    private float maxDuration;
    private float duration;
    private float timePassed;

    public float Cooldown {
        get {
            return maxDuration;
        }
    }

    public void Setup(int level, int direction) {
        sr = gameObject.GetComponent<SpriteRenderer>();
        if(direction < 0) {
            sr.flipX = true;
        }
        this.level = level;
        sr.sprite = sprites[level];
        duration = maxDuration;
        if (level > 0) {
            duration *= 2;
        }

        timePassed = 0;
    }

    void Update()
    {
        timePassed += Time.deltaTime;

        if(timePassed > duration) {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        GameObject other = collision.gameObject;
        Rigidbody2D rb2d = other.GetComponent<Rigidbody2D>();
        if(other.tag.Equals("Projectile")) {
            if(level == 0) {
                rb2d.velocity = new Vector2(0, rb2d.velocity.y);
            } else if(level == 1) {
                rb2d.velocity = new Vector2(-0.5f * rb2d.velocity.x, rb2d.velocity.y);
            } else if(level == 2) {
                rb2d.velocity = new Vector2(-1 * rb2d.velocity.x, rb2d.velocity.y);
            }
        }
    }
}
