﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    [SerializeField]
    private List<Button> menuButtons;
    [SerializeField]
    private GameObject controlsMenu;
    [SerializeField]
    private GameObject uiSound;

    // Start is called before the first frame update
    void Start()
    {
        Application.targetFrameRate = 60;
        HideControls();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlayGame() {
        SceneManager.LoadScene(1);
    }

    public void QuitGame() {
        Application.Quit();
    }

    public void ShowControls() {
        foreach(Button b in menuButtons) {
            b.interactable = false;
        }

        controlsMenu.SetActive(true);
    }

    public void HideControls() {
        foreach (Button b in menuButtons) {
            b.interactable = true;
        }

        controlsMenu.SetActive(false);
    }

    public void PlayUiSound() {
        Instantiate(uiSound, transform.position, Quaternion.identity);
    }
}
