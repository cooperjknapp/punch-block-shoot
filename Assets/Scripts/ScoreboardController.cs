﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreboardController : MonoBehaviour
{
    [SerializeField]
    private Sprite[] sprites;
    private List<SpriteRenderer> boxes;
    private int[] scoreboard;

    void Start()
    {
        boxes = new List<SpriteRenderer>();
        for(int i = 0; i < 5; i++) {
            boxes.Add(GameObject.Find("B"+i).GetComponent<SpriteRenderer>());
        }
        scoreboard = new int[2];
        scoreboard[0] = 0;
        scoreboard[1] = 0;
    }

    public void ScorePlayer(int player) {
        scoreboard[player]++;
        HandleSprites();
    }

    private void HandleSprites() {
        for(int i = 0; i < scoreboard[0]; i++) {
            boxes[i].sprite = sprites[1];
        }

        for(int i = boxes.Count - 1; i >= boxes.Count - scoreboard[1]; i--) {
            boxes[i].sprite = sprites[2];
        }
    }
}
