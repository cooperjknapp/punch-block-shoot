﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionController : MonoBehaviour
{
    [SerializeField]
    private float maxSize;
    private SpriteRenderer sr;
    private float size;
    [SerializeField]
    private float sizeAccel;


    // Start is called before the first frame update
    void Start()
    {
        size = 0.2f;
    }

    // Update is called once per frame
    void Update()
    {
        if (size >= maxSize) {
            Destroy(gameObject);
        }

        size *= sizeAccel * (Time.deltaTime + 1);

        transform.localScale = new Vector2(size, size);
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.gameObject.tag.Equals("Player")) {
            collision.gameObject.GetComponent<CharacterController>().GetHitBy(3, transform.position);
        }
    }
}
