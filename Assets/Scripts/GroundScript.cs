﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundScript : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision) {
        if(collision.gameObject.tag.Equals("Player")) {
            collision.gameObject.GetComponent<CharacterController>().SetOnGround(true);
        }
    }

    private void OnTriggerStay2D(Collider2D collision) {
        if (collision.gameObject.tag.Equals("Player")) {
            collision.gameObject.GetComponent<CharacterController>().SetOnGround(true);
        }
    }

    private void OnTriggerExit2D(Collider2D collision) {
        if (collision.gameObject.tag.Equals("Player")) {
            collision.gameObject.GetComponent<CharacterController>().SetOnGround(false);
        }
    }
}
