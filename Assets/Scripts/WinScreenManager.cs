﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinScreenManager : MonoBehaviour
{
    [SerializeField]
    List<GameObject> winScreens;
    DontDestroyData dontDestroyData;
    private float timer;

    void Start()
    {
        dontDestroyData = GameObject.Find("DontDestroyData").GetComponent<DontDestroyData>();
        winScreens[dontDestroyData.winner].SetActive(true);
        timer = 3;
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;
        if(timer <= 0 && Input.anyKeyDown) {
            SceneManager.LoadScene(0);
        }
    }
}
